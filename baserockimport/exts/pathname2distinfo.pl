#!/usr/bin/perl
#
# A helper utility, takes a Perl distribution pathname and outputs
# a JSON representation of the distribution data.
#
# Copyright © 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use 5.020;
use strict;
use English;
use warnings;

use CPAN::DistnameInfo;
use JSON;

my $argc = @ARGV;

unless ($argc == 1) {
    say STDERR "usage: $PROGRAM_NAME <pathname>";
    exit 1;
}

my $pathname = $ARGV[0];
my $dist_info = CPAN::DistnameInfo->new($pathname);

unless (defined $dist_info) {
    die "Couldn't construct DistInfo from pathname: $pathname";
}

my %h = $dist_info->properties;
my $json = encode_json(\%h);

say $json;
