README
------

The CPAN importer uses carton
(http://search.cpan.org/~miyagawa/Carton-v1.0.22/lib/Carton.pm)
to obtain a perl distribution's dependency set, it also injects modules
into a local minicpan repository to make it possible to run imports based
on locally modified sources as well as sources pulled straight from CPAN.

To import a module,

    baserock-import cpan IO::Async

optionally you can supply a version,

    baserock-import cpan IO::Async 0.68

if anything goes wrong the log will be useful, run with --log=<path>
to log to a file. Since carton fetches sources with cpanm you may also
find the cpanm log useful, by default this is located at ~/.cpanm/build.log
cpanm also stores any tarballs it fetches into this directory, if you're
importing very large distributions (with hundreds of dependencies)
then you may want to set the PERL_CPANM_HOME environment variable when you
run the import, e.g.

    PERL_CPANM_HOME=/src/cpanm_data baserock-import cpan IO::Async

to ensure cpanm doesn't run out of disk space during the import.

Generally any perl distribution that can be installed with cpanm can
be imported with the import tool, if you have any difficulties or
find any distributions that can't be imported please send a mail
to baserock-dev@baserock.org or ask in #baserock on irc.freenode.net

HACKING
-------

If you're modifying any part of the CPAN extensions,
then please run the `run_cpan_test_imports.pl' script
before and after your modification, ensuring that all modules that imported
successfully prior to your modification also import successfully aftewards,
this extension has a lot of corner cases so regressions are easily introduced.
